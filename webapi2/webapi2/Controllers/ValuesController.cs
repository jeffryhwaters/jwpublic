﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace webapi2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
    
   


            // GET api/values/5
            [HttpGet("{stringValue}")]
            public ActionResult<string> Get(string stringValue)
            {
                return "You asked for " + stringValue;
            }


       
    }
}
